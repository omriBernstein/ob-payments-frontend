// Adapted from https://github.com/rehooks/local-storage

import { useEffect, useState, useCallback } from 'react';

const tryParse = value => {
  try {
    return JSON.parse(value);
  } catch {
    return value;
  }
}

class LocalStorageChanged extends CustomEvent {
  static eventName = 'onLocalStorageChange';
  constructor (payload) {
    super(LocalStorageChanged.eventName, {
      detail: payload
    });
  }
}

const writeToStorage = (key, value) => {
  try {
    const serializedValue = typeof value === 'object' ? JSON.stringify(value) : `${value}`;
    localStorage.setItem(key, serializedValue);
    window.dispatchEvent(new LocalStorageChanged({key, value}));
  } catch(err) {
    if (err instanceof TypeError && err.message.includes('circular structure')) {
        throw new TypeError(
            'The object that was given to the writeStorage function has circular references.\n' +
            'For more information, check here: ' +
            'https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Errors/Cyclic_object_value'
        );
    }
    throw err;
  }
};
const deleteFromStorage = key => {
  localStorage.removeItem(key);
  window.dispatchEvent(new LocalStorageChanged({key, value: undefined}));
};

const useLocalStorage = (key, initialValue) => {
  const [localState, updateLocalState] = useState(tryParse(localStorage.getItem(key)));

  const onLocalStorageChange = useCallback(event => {
    if (event instanceof LocalStorageChanged) {
      if (event.detail.key === key) {
        updateLocalState(event.detail.value);
      }
    } else {
      if (event.key === key) {
        updateLocalState(event.newValue);
      }
    }
  }, [key]);

  useEffect(() => {
    window.addEventListener(
      LocalStorageChanged.eventName,
      onLocalStorageChange
    );
    window.addEventListener('storage', onLocalStorageChange);
    if (initialValue) writeToStorage(key, initialValue);
    return () => {
      window.removeEventListener(
        LocalStorageChanged.eventName,
        onLocalStorageChange
      );
      window.removeEventListener('storage', onLocalStorageChange);
    };
  }, [key, initialValue, onLocalStorageChange]);

  return [localState, (value) => writeToStorage(key, value), () => deleteFromStorage(key)];
}

export default useLocalStorage;
