const formatDate = rawString => {
  return new Date(rawString).toDateString();
};

export default formatDate;
