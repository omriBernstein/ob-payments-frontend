import React from 'react';
import {Redirect} from 'react-router-dom';

import useLoggedIn from './use-logged-in';

const withGatekeeper = component => {
  const GatekeeperWrapper = props => {
    const isLoggedIn = useLoggedIn();
    if (!isLoggedIn) return <Redirect to='/' />;
    return React.createElement(component, props);
  };
  return GatekeeperWrapper;
};

export default withGatekeeper;
