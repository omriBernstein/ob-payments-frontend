import {useMemo} from 'react';
import axios from 'axios';

import useLocalStorage from './use-local-storage';
import {authStorageKey} from '../constants';

const useAuthenticatedAxios = () => {
  const [authToken, setAuthToken] = useLocalStorage(authStorageKey);
  const authenticatedAxios = useMemo(
    () => axios.create({
      headers: {Authorization: `JWT ${authToken}`}
    }),
    [authToken]
  );
  authenticatedAxios.interceptors.response.use(null, error => {
    if (error.response.status === 401) {
      setAuthToken('');
    }
  });
  return authenticatedAxios;
};

export default useAuthenticatedAxios;
