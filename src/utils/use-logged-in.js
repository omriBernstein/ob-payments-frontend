import useLocalStorage from './use-local-storage';
import {authStorageKey} from '../constants';

const useLoggedIn = () => {
  const [authToken] = useLocalStorage(authStorageKey);
  return Boolean(authToken);
};

export default useLoggedIn;
