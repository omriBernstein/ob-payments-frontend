import React from 'react';
import ReactDOM from 'react-dom';
import Root from './components/Root';
import 'semantic-ui-css/semantic.min.css';

ReactDOM.render(<Root />, document.getElementById('root'));

