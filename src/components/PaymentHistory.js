import React, {Fragment, useState, useEffect} from 'react';
import {Header, Loader} from 'semantic-ui-react';

import useAuthenticatedAxios from '../utils/use-authenticated-axios';
import TransactionList from './TransactionList';

const PaymentHistory = () => {
  const axios = useAuthenticatedAxios();
  const [sentTranscations, setSentTranscations] = useState();
  const [receivedTransactions, setReceivedTransactions] = useState();
  useEffect(() => {
    const loadData = async () => {
      const {data: newSentTranscations} = await axios.get('/api/v1/payments/sent_transactions/');
      setSentTranscations(newSentTranscations);
      const {data: newReceivedTranscations} = await axios.get('/api/v1/payments/received_transactions/');
      setReceivedTransactions(newReceivedTranscations);
    };
    loadData();
  }, [axios]);
  return (
    <Fragment>
      <Header size='medium'>Sent Transactions</Header>
      {sentTranscations === undefined
        ? <Loader>Loading</Loader>
        : <TransactionList transactions={sentTranscations} sentOrReceived='sent' />
      }
      <Header size='medium'>Received Transactions</Header>
      {receivedTransactions === undefined
        ? <Loader>Loading</Loader>
        : <TransactionList transactions={receivedTransactions} sentOrReceived='received' />
      }
    </Fragment>
  );
};

export default PaymentHistory;
