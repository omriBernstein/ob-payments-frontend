import React from 'react';
import {Segment} from 'semantic-ui-react';
import {Link} from 'react-router-dom';

import formatDate from '../utils/format-date';

const walletPropMap = {
  sent: 'to_wallet',
  received: 'from_wallet'
};

const verbMap = {
  sent: 'sent to',
  received: 'received from'
};

const TransactionList = ({transactions, sentOrReceived}) => {
  return (
    <Segment.Group>
      {transactions.length === 0
        ? <Segment>(You have none)</Segment>
        : transactions.map(transaction => {
          const {id, amount, created_at} = transaction;
          const wallet = transaction[walletPropMap[sentOrReceived]];
          return (
            <Segment key={id}>
              ${amount} {verbMap[sentOrReceived]} <Link to={`/user/${wallet.user.id}`}>{wallet.user.username}</Link> on {formatDate(created_at)}
            </Segment>
          );
        })
      }
    </Segment.Group>
  )
};

export default TransactionList;
