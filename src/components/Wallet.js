import React, {useState, useEffect} from 'react';
import {Statistic, Loader} from 'semantic-ui-react';

import useAuthenticatedAxios from '../utils/use-authenticated-axios';

const Dashboard = () => {
  const axios = useAuthenticatedAxios();
  const [walletBalance, setWalletBalance] = useState();
  useEffect(() => {
    const loadData = async () => {
      const {data: {balance: newWalletBalance}} = await axios.get('/api/v1/payments/wallet/');
      setWalletBalance(newWalletBalance);
    };
    loadData();
  }, [axios]);
  return (
    <Statistic>
      <Statistic.Label>Your Wallet</Statistic.Label>
      <Statistic.Value>
        {walletBalance === undefined
          ? <Loader>Loading</Loader>
          : `$${walletBalance}`
        }
      </Statistic.Value>
    </Statistic>
  );
};

export default Dashboard;

