import React, {Fragment} from 'react';
import {Form, Button, Header} from 'semantic-ui-react';
import axios from 'axios';
import {Redirect} from 'react-router-dom';

import useLocalStorage from '../utils/use-local-storage';
import {authStorageKey} from '../constants';

const Authenticate = ({label}) => {
  const [authToken, setAuthToken] = useLocalStorage(authStorageKey);
  if (authToken) return <Redirect to='/dashboard' />;
  const onSubmit = async event => {
    const data = {
      username: event.target.username.value,
      password: event.target.password.value
    };
    const {data: {token}} = await axios.post('/api-token-auth/', data);
    setAuthToken(token);
  };
  return (
    <Fragment>
      <Header size='large'>{label}</Header>
      <Form onSubmit={onSubmit}>
        <Form.Input
          icon='user'
          iconPosition='left'
          label='Username'
          placeholder='Username'
          name='username' />
        <Form.Input
          icon='lock'
          iconPosition='left'
          label='Password'
          type='password'
          placeholder='Password'
          name='password' />
        <Button primary content={label} type='submit' />
      </Form>
    </Fragment>
  );
};

export default Authenticate;

