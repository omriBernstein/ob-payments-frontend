import React from 'react';

import Authenticate from './Authenticate';

const Signin = () => <Authenticate label='Signin' />;

export default Signin;

