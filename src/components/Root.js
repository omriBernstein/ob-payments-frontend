import React from 'react';
import {Container} from 'semantic-ui-react';
import {BrowserRouter} from 'react-router-dom';

import Main from './Main';
import MenuBar from './MenuBar';

const Root = () => {
  return (
    <BrowserRouter>
      <Container>
        <MenuBar />
        <Main />
      </Container>
    </BrowserRouter>
  );
}

export default Root;
