import React from 'react';
import {Menu} from 'semantic-ui-react';
import {NavLink, withRouter} from 'react-router-dom';

import UserSearch from './UserSearch';
import useLoggedIn from '../utils/use-logged-in';

const MenuBar = ({location}) => {
  const isLoggedIn = useLoggedIn();
  return (
    <Menu secondary pointing>
      <Menu.Item active={location.pathname === '/'}>
        <NavLink to='/'>Home</NavLink>
      </Menu.Item>
      {isLoggedIn && (
        <Menu.Item active={location.pathname === '/dashboard'}>
          <NavLink to='/dashboard'>Dashboard</NavLink>
        </Menu.Item>
      )}
      {isLoggedIn && (
        <Menu.Item position='right'>
          <UserSearch />
        </Menu.Item>
      )}
    </Menu>
  );
};

export default withRouter(MenuBar);
