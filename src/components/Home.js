import React, {Fragment} from 'react';
import {Link} from 'react-router-dom';
import {Header, List, Button} from 'semantic-ui-react';

import useLoggedIn from '../utils/use-logged-in';
import Signout from './Signout';

const Home = () => {
  const isLoggedIn = useLoggedIn();
  return (
    <Fragment>
      <Header size='large'>Home</Header>
      <List horizontal>
        {isLoggedIn
          ? <List.Item><Signout /></List.Item>
          : (
            <Fragment>
              <List.Item>
                <Link to='/signin'>
                  <Button content='Signin' icon='sign-in' size='medium' />
                </Link>
              </List.Item>
              <List.Item>OR</List.Item>
              <List.Item>
                <Link to='/signup'>
                  <Button content='Signup' icon='signup' size='medium' />
                </Link>
              </List.Item>
            </Fragment>
          )
        }
      </List>
    </Fragment>
  );
};

export default Home;

