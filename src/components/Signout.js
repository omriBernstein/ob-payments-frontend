import React from 'react';
import {Button} from 'semantic-ui-react';

import useLocalStorage from '../utils/use-local-storage';
import {authStorageKey} from '../constants';

const Signout = () => {
  const [, setAuthToken] = useLocalStorage(authStorageKey);
  const onClick = () => setAuthToken('');
  return (
    <Button content='Signout' icon='sign-out' size='medium' onClick={onClick} />
  );
};

export default Signout;

