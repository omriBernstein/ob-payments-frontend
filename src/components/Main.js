import React from 'react';
import {Route, Switch, Redirect} from 'react-router-dom';

import Home from './Home';
import Signup from './Signup';
import Signin from './Signin';
import Dashboard from './Dashboard';
import UserDetail from './UserDetail';

const Main = () => {
  return (
    <Switch>
      <Route exact path='/' component={Home} />
      <Route exact path='/signup' component={Signup} />
      <Route exact path='/signin' component={Signin} />
      <Route exact path='/dashboard' component={Dashboard} />
      <Route exact path='/user/:userId' component={UserDetail} />
      <Redirect to='/' />
    </Switch>
  );
}

export default Main;
