import React, {useState, useEffect, useCallback} from 'react';
import {withRouter} from 'react-router-dom';
import {Modal, Button, Form, Message} from 'semantic-ui-react';
import debounce from 'lodash.debounce';

import useAuthenticatedAxios from '../utils/use-authenticated-axios';

const SendMoney = ({username, wallet, history}) => {
  const axios = useAuthenticatedAxios();
  const [amount, setAmount] = useState('0.00');
  const [error, setError] = useState();
  const [isModalOpen, setIsModalOpen] = useState(false);
  const openModal = () => setIsModalOpen(true);
  const closeModal = () => setIsModalOpen(false);
  const handleChange = event => {
    setError(undefined);
    setAmount(event.target.value);
  }
  const updateToDecimal = useCallback(
    debounce(currentAmount => {
      if (typeof currentAmount === 'string' && currentAmount.slice(-3).startsWith('.')) return;
      setAmount(Number(currentAmount).toFixed(2));
    }, 1000),
    []
  );
  useEffect(() => {
    updateToDecimal(amount);
    return () => {
      updateToDecimal.cancel();
    };
  }, [amount, updateToDecimal]);
  const handleSubmit = async () => {
    try {
      await axios.post('/api/v1/payments/send_payment/', {
        amount,
        to_wallet: wallet.id
      });
      closeModal();
    } catch (err) {
      setError(err.response.data);
    }
  };
  return (
    <Modal
      closeIcon
      size='mini'
      open={isModalOpen}
      trigger={<Button content='Send money' icon='dollar' size='medium' onClick={openModal} />}
      onClose={closeModal}>
      <Modal.Header>Send money to {username}</Modal.Header>
      <Modal.Content>
        {error && (
          <Message negative>
            <Message.Header>Failed to send</Message.Header>
            {Object.keys(error).map(key => (
              <p key={key}>{key}: {error[key]}</p>
            ))}
          </Message>
        )}
        <Form onSubmit={handleSubmit}>
          <Form.Input
            placeholder='Amount'
            width={10}
            label='Amount'
            icon='dollar'
            iconPosition='left'
            type='number'
            step={0.01}
            min={0}
            value={amount}
            onChange={handleChange} />
          <Button primary content='Send' type='submit' />
        </Form>
      </Modal.Content>
    </Modal>
  );
};

export default withRouter(SendMoney);
