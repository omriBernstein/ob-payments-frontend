import React, {useState, useEffect, useCallback} from 'react';
import {withRouter} from 'react-router-dom';
import {Dropdown} from 'semantic-ui-react';
import debounce from 'lodash.debounce';

import useAuthenticatedAxios from '../utils/use-authenticated-axios';

const UserSearch = ({history}) => {
  const axios = useAuthenticatedAxios();
  const [searchQuery, setSearchQuery] = useState('');
  const [options, setOptions] = useState([]);
  const [value, setValue] = useState();
  const [isFetching, setIsFetching] = useState(false);
  const handleSearchChange = (_evt, {searchQuery: newSearchQuery}) => setSearchQuery(newSearchQuery);
  const handleChange = (_evt, {value: newValue}) => setValue(newValue);
  const loadOptions = useCallback(
    debounce(async searchQuery => {
      if (searchQuery.length === 0) {
        setOptions([]);
        return;
      }
      setIsFetching(true);
      const {data} = await axios.get('/api/v1/social/users/', {params: {search: searchQuery}});
      const newOptions = data.map(({id, username}) => ({
        key: id,
        text: username,
        value: id
      }));
      setOptions(newOptions);
      setIsFetching(false);
    }, 500),
    [axios]
  );
  useEffect(() => {
    loadOptions(searchQuery);
    return () => {
      loadOptions.cancel();
    };
  }, [loadOptions, searchQuery]);
  useEffect(() => {
    if (value === undefined) return;
    history.push(`/user/${value}`);
    setValue(undefined);
    setSearchQuery('');
  }, [history, value]);
  return (
    <Dropdown
      inline
      search
      selection
      placeholder='Search users...'
      icon='search'
      options={options}
      searchQuery={searchQuery}
      value={value}
      onChange={handleChange}
      onSearchChange={handleSearchChange}
      disabled={isFetching}
      loading={isFetching} />
  );
};

export default withRouter(UserSearch);
