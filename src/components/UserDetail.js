import React, {useState, useEffect, Fragment} from 'react';
import {Header, Loader} from 'semantic-ui-react';

import useAuthenticatedAxios from '../utils/use-authenticated-axios';
import withGatekeeper from '../utils/with-gatekeeper';
import SendMoney from './SendMoney';

const UserDetail = ({match: {params: {userId}}}) => {
  const axios = useAuthenticatedAxios();
  const [userInfo, setUserInfo] = useState();
  const [selfInfo, setSelfInfo] = useState();
  useEffect(() => {
    const loadData = async () => {
      const {data: newSelfInfo} = await axios.get('/api/v1/social/user/');
      setSelfInfo(newSelfInfo);
      const {data: newUserInfo} = await axios.get(`/api/v1/social/users/${userId}/`);
      setUserInfo(newUserInfo);
    };
    loadData();
  }, [axios, userId]);
  if (!userInfo || !selfInfo) return <Loader>Loading...</Loader>;
  return (
    <Fragment>
      <Header size='large'>{userInfo.username}</Header>
      {(userInfo.first_name || userInfo.last_name) && (
        <p>({userInfo.first_name} {userInfo.last_name})</p>
      )}
      {userInfo.id === selfInfo.id
        ? 'You cannot send money to yourself'
        : <SendMoney {...userInfo} />
      }
    </Fragment>
  );
};

export default withGatekeeper(UserDetail);
