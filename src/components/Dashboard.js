import React, {Fragment} from 'react';
import {Header, Segment} from 'semantic-ui-react';

import Wallet from './Wallet';
import PaymentHistory from './PaymentHistory';
import withGatekeeper from '../utils/with-gatekeeper';

const Dashboard = () => {
  return (
    <Fragment>
      <Header size='large'>Dashboard</Header>
      <Segment compact>
        <Wallet />
      </Segment>
      <PaymentHistory />
    </Fragment>
  );
};

export default withGatekeeper(Dashboard);

