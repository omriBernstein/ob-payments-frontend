import React from 'react';

import Authenticate from './Authenticate';

const Signup = () => <Authenticate label='Signup' />;

export default Signup;

