This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode. Open http://localhost:3000 to view it in the browser. This particular web application is coupled with a corresponding [backend](https://bitbucket.org/newtonx-team/payments_demo), which it expects to proxy requests to at http://localhost:8000. For the frontend to fully work, you must also download and start that backend locally.

The page will reload if you make edits. You will also see any lint errors in the console.

### `npm run build`

Builds the app for production to the `build` folder. It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes. Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.
